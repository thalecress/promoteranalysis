DEPRECATED.  Please see https://bitbucket.org/jnmaloof/motifenrichment

WARNING! THIS ONLY SEARCHES ON ONE STRAND OF THE DNA

README for Promoter Analysis
========================================================

This is a collection of scripts for performing promter analysis in tomato.

Current functionality is limited to searching for enrichment of known promoter motifs among differentially expressed genes.

Files
--------------------------------------------------------
* MatchKnownMotifs.R *R script that provides the over-representation analysis*
* element_name_and_motif_IUPACsupp.txt *known motif file*
* SlITAG2.3Upstream1000.fa *1000bp upstream of the ITAG2.3 cDNAs*
* 20120801.IL9.2.SLY_vs_IL9.2.all.pvals.annot.csv *file of p-values to demonstrate functionality*

To-DOs
-----------------------------------------------------------
* add script to search for novel motifs
* add script for extracting promoters
* add script to compare S. lycopersicum and S. pennellii promoters